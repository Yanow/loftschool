<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Практикум №5</title><link rel="stylesheet" href="style.css"></head><body><h1>Практикум №5 - "Масиивы"</h1><a href=".">вернуться</a><hr>

<?php

$arr1 = Array();
$arr2 = Array(1,2,3);
$arr3 = [4,5,6];

echo $arr1.'<br>';

echo '<pre>';
print_r($arr2);
print_r($arr3);
echo '</pre>';

echo join(' | ',$arr2), '<br>';

$str = 's1 s2 s3 s4';
echo $str,'<br>';
$arr = explode('s',$str);
echo '<pre>';
print_r($arr);
echo '</pre>';

$aa = Array(
        'name' => 'Сергей',
        'age' => 25
    );
echo '<pre>';
print_r($aa);
echo '</pre>';

$aa = Array(
        'name' => 'Сергей1',
        'age' => 25,
        'deep' => Array(
            'name' => 'Сергей2',
            'age' => 25,
            'deep' => Array(
                'name' => 'Сергей3',
                'age' => 25,
                'deep' => Array(
                    'name' => 'Сергей4',
                    'age' => 25,
                    'deep' => Array(
                        'name' => 'Сергей5',
                        'age' => 25
                    )
                )
            )
        )
    );
echo '<pre>';
print_r($aa);
echo '</pre>';

?>

</body>
</html>