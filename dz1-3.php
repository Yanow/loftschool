<!DOCTYPE html><html lang="en">
<head>
    <meta charset="UTF-8">
      <title>Домашнее заданее №1-3</title>
<link rel="stylesheet" href="style.css">
</head><body><h>Домашнее заданее №1-3</h1><a href=".">вернуться</a><hr>
<?php
echo 'Создайте константу и присвойте ей значение. Проверьте, существует ли константа, 
которую Вы хотите использовать. Выведите значение данной константы. Попытайтесь 
изменить значение созданной константы. Получилось?', "<br>", "<br>";

define("CONSTANTA", '3.14', true);

echo 'CONSTANTA =', CONSTANTA, '<br>'; 

define("CONSTANTA", '3.217');

echo 'CONSTANTA =', CONSTANTA;

