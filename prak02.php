<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Практикум №2</title><link rel="stylesheet" href="style.css"></head><body><h1>Практикум №2 - "Переменные"</h1><a href=".">вернуться</a><hr>
<?php

echo '<h4>Переменные</h4>';

$bool = true; // Логическая. true/false
$int = 5; // integer
$real = 3.4; // float
$str = 'Строка'; // string

echo $bool,' - ',gettype($bool),'<br>';
echo $int,' - ',gettype($int),'<br>';
echo $real,' - ',gettype($real),'<br>';
echo $str,' - ',gettype($str),'<br>';

echo '<h4>Арифметические операции</h4>';

$a = 5; $b = 3;
echo '$a = ',$a,'; $b = ',$b,';<br>';
echo '-$a = ',-$a,';<br>';
echo '$a + $b = ', $a + $b, ';<br>';
echo '$a - $b = ', $a - $b, ';<br>'; 
echo '$a * $b = ', $a * $b, ';<br>';
echo '$a / $b = ', $a / $b, ';<br>';
echo '$a % $b = ', $a % $b, ';<br>';

echo 'pow($a,$b) = ', pow($a,$b) , ';<br>';

echo '<h4>Инкремент/декримент</h4>';

echo '$a = ',"$a",'; ++$a = ', ++$a,'; $a++ = ', $a++ ,'; итог: $a = ', $a , '<br>';
echo '$b = ',$b,'; --$b = ', --$b,'; $b-- = ', $b-- ,'; итог: $b = ', $b , '<br>';

echo '<h4>Строковые переменные</h4>';

$str1 = 'Строка №1'; echo $str1, '<br>';
$str2 = "Строка №2"; echo $str2, '<br>';

$str12 = $str1.$str2;
echo 'Объединение: $str1.$str -> ', $str12, '<br>';

echo "Экранирование в строке \"с кавычками\" внутри строки<br>";
echo 'Экранирование в строке \'с апострафами\' внутри строки<br>';

$c = 25;
echo "Пременная \$c = $c внутри строки с кавычками<br>";
echo 'Пременная \$c = $c внутри строки с кавычками<br>';

?>

</body>
</html>