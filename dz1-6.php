<!DOCTYPE html><html lang="en">
<head>
    <meta charset="UTF-8">
      <title>Домашнее заданее №1-5</title>
<link rel="stylesheet" href="style.css">
</head><body><h>Домашнее заданее №1-5</h1><a href=".">вернуться</a><hr>
<?php
echo 'Создайте массив $bmw с ячейками: model, speed, doors, year.
Заполните ячейки значениями соответственно: “X5”, 120, 5, “2015”. 
Создайте массивы $toyota и $opel аналогичные массиву $bmw (заполните данными).
Выведите значения всех трёх массивов в виде:
Модель: model
Скорость: speed
Двери: doors
Год выпуска: year
Например:
Модель: X5 Скорость: 120 Двери: 5
Год выпуска: 2015', "<br>", "<br>";

$bmw = Array (
        'model' => 'X5',
        'speed' => '120',
        'doors' => '5',
        'year'  => '2015',
  );

$toyota = Array (
        'model' => 'camry',
        'speed' => '180',
        'doors' => '4',
        'year'  => '2016'
  );

$opel = Array (
        'model' => 'astra',
        'speed' => '150',
        'doors' => '3',
        'year'  => '2013'
  );
  
//bmw
$mod_bmw = $bmw['model'];
$speed_bmw = $bmw['speed'];
$doors_bmw = $bmw['doors'];
$year_bmw = $bmw['year'];
  
//toyota
$mod_toyota = $toyota['model'];
$speed_toyota = $toyota['speed'];
$doors_toyota = $toyota['doors'];
$year_toyota = $toyota['year'];

//opel

$mod_opel = $opel['model'];
$speed_opel = $opel['speed'];
$doors_opel = $opel['doors'];
$year_opel = $opel['year'];


echo 'Автомобиль BMW', '<br>';
echo 'Модель:', $mod_bmw, '<br>','Скорость:', $speed_bmw, '<br>', 'Двери:', $doors_bmw, '<br>', 'Год:', $year_bmw, '<br>','<br>';

echo 'Автомобиль toyota', '<br>';
echo 'Модель:', $mod_toyota, '<br>','Скорость:', $speed_toyota, '<br>', 'Двери:', $doors_toyota, '<br>', 'Год:', $year_toyota, '<br>','<br>';

echo 'Автомобиль opel', '<br>';
echo 'Модель:', $mod_opel, '<br>','Скорость:', $speed_opel, '<br>', 'Двери:', $doors_opel, '<br>', 'Год:', $year_opel, '<br>','<br>';