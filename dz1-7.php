<!DOCTYPE html><html lang="en">
<head>
    <meta charset="UTF-8">
      <title>Домашнее заданее №1-5</title>
<link rel="stylesheet" href="style.css">
</head><body><h>Домашнее заданее №1-7</h1><a href=".">вернуться</a><hr>
<?php
echo 'Создать ассоциативный массив $cars. Данные взять из задания No6.
Требуется, чтобы все данные были в одном массиве. Реализовать через
вложенные массивы. Вывести массив в удобочитаемом виде через конструкцию 
print_r и через foreach.', "<br>", "<br>";


 $cars =  Array (
   
           'bmw' => Array (
                    'model' => 'X5',
                    'speed' => '120',
                    'doors' => '5',
                    'year'  => '2015'),
        'toyota' => Array (
                    'model' => 'camry',
                    'speed' => '180',
                    'doors' => '4',
                    'year'  => '2016'),

          'opel' => Array (
                    'model' => 'astra',
                    'speed' => '150',
                    'doors' => '3',
                    'year'  => '2013'),
                );
                
echo '<pre>';
print_r($cars);
echo '</pre>';

foreach ($cars as $key => $value) {
  
  echo $key.': '.'Модель:'.$value['model'].', '.'Скорость:'.$value['speed'].', '.'Двери:'.$value['doors'].', '.'Год:'.$value['year'].'<br>';
  
}

