<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Практикум №3</title><link rel="stylesheet" href="style.css"></head><body><h1>Практикум №3 - "Константы"</h1><a href=".">вернуться</a><hr>

<?php

echo '<h4>Константы</h4>';

define("CONST1", 'значение', true);
//const CONST2 = "Здравствуй, мир."; //до версии PHP 5.3 разрешалось только в классах

echo 'CONST1 = ', "CONST1", '<br>';
//echo 'CONST2 = ', CONST2, '<br>';
echo 'const1 = ', "const1", '<br>';

//CONST1 = 'новое значение';
//echo 'CONST1 = ', CONST1, '<br>';

define("CONST1",'новое значение');
echo 'CONST1 = ', CONST1, '<br>';

// define("CONST1", 'новое значение 2');
// echo 'CONST1 = ', CONST1, '<br>';

?>

</body>
</html>