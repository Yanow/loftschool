<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Практикум №6</title><link rel="stylesheet" href="style.css"></head><body><h1>Практикум №6 - "Циклы"</h1><a href=".">вернуться</a><hr>

<?php

echo 'FOR: ';

for($i = 0; $i < 20; $i+=2){
    echo $i.'<br>';
}

echo '<br>WHILE:<br>';

$i = 20;
while ($i < 10) {
    echo $i.'<br>';
    $i++;
}

echo '<br>DO-WHILE: ';

$i = 20;
do{
    echo $i.'<br>';
    $i++;
    ++$i;

}while ($i < 10);

echo 'FOR + continue: ';

for($i = 0; $i < 10; $i++){
    if ($i % 2 == 1) continue;
    echo $i;
}

echo 'FOR + break: ';

for($i = 0; $i < 10; $i++){
    if ($i == 5) break;
    echo $i;
}

echo 'FOR + break N:<br>';

for($i = 0; $i < 4; $i++){
    for($j = 0; $j < 4; $j++){
        for($k = 0; $k < 4; $k++){
            if ($k == 2) break 3;
            echo '$i=',$i.'; $j=',$j,'; $k=',$k,'<br>';
        }
    }
}

?>

</body>
</html>